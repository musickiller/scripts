function what_is_this -a first second                                                   ─╯
    if test "$first" = "second"
        what is this $second
    else if test -z $first
        echo "zero strhing"
    else if test $first = ""
        echo "empty string"
    else if test $first = " "
        echo space
    else if test -n $first
        echo "other non-empty string"
    end
end

what_is_this $argv
