function test_is_none_or_empty -a string_to_test enable_debug
    # 0 = OK, empty
    # 1 = non-empty
    if test -z $string_to_test
        return 0
    else if test ( string trim $string_to_test ) = ""
        return 0
    else
        return 1
    end
end
