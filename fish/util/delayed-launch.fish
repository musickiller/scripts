#!/bin/fish
set process_name $argv[1]
set pid ( pgrep $process_name )
echo monitoring process $process_name, pid $pid

# quotes are needed for -n to work.. for some reason o_O
while test -n "$( pgrep $process_name )"
    echo still runs
    sleep 15
end

echo process completed!
