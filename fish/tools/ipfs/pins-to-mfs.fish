#!/bin/fish

# IMPORTS
set script_dir ( dirname ( status -f ))
source $script_dir/ipfs_list_pins.fish

if contains -- -h $argv
    echo "
argv[1] is expected to be path. Can be empty to start from root
!!! Conflicts with flags for now, sorry!

-m  to remove the pin
    not sure what that does in the end, since there is no pin now,
    but it is in files, right?

    Don't do that in general, as you probably still have something
    you'd like to keep as pin, no?

    after testing:
    ugh... I'm not sure this works, actually o_O Or rather it does, but not always
"
    return 0
end

set path $argv[1]

set pins ( ipfs_list_pins )
for pin in $pins
    ipfs files cp -p /ipfs/$pin $path/ \
    && if contains -- -m $argv
        ipfs pin rm $pin
    end
end
