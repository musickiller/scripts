#!/bin/fish

set script_dir ( dirname ( status -f ))
source $script_dir/test_is_none_or_empty.fish
source $script_dir/echo_list.fish


function ipfs_list_pins \
        -d "better way of showing pins that bother me =D
                options: --show-types -t show types of pins"
        
    set recurse_pins ( ipfs pin ls --type=recursive )
    set direct_pins ( ipfs pin ls --type=direct )
    set all_pins $recurse_pins $direct_pins

    if contains -- -t $argv or contains -- --show-types $argv
        echo_list $all_pins

        return 0
    end

    set -e clean_pins
    for pin in $all_pins
        set clean_pins $clean_pins ( string split " " $pin )[1]
    end

    #echo $clean_pins
    echo_list $clean_pins
end


not status --is-block; and ipfs_list_pins $argv
