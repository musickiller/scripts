#!/bin/fish

set script_dir ( dirname ( status -f ))
source $script_dir/test_is_none_or_empty.fish
source $script_dir/ipfs_list_pins.fish


function list_pins
    set pins ( ipfs_list_pins )
    echo current pins:
    echo ""

    for i in (seq (count $pins))
        echo $i $pins[$i]
    end

    echo ""
    echo select pin to display or nothing to quit
    echo Please use a correct number, there is no fool proofing here =P
    read selection
    
    if test_is_none_or_empty $selection
        return 0
    end
    
    set hash $pins[$selection]
    ls_pin_contents $hash
end


function ls_pin_contents -a hash
    echo listing hash $hash
    echo ""
    ipfs ls $hash
    echo actions: [u]npin [B]ack
    read action
    
    if test ( string lower $action ) = u ||
            test ( string lower $action ) = unpin
        ipfs pin rm $hash
    end
    
    list_pins
end


list_pins
