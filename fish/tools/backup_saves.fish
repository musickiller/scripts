#!/bin/fish

set save_dir "My Summer Car"
set backup_dir save_backups

mkdir -p $backup_dir

set nr_backups ( ls $backup_dir | count )

cp --reflink=always -r $save_dir $backup_dir/$nr_backups
