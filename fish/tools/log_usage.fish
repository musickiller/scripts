#!/bin/fish

function get_data
    argparse 'c/cpu' 'm/mem' 'n/name' 'p/pid' -- $argv
    
    set PID_COL "\$1"
    set CPU_COL "\$9"
    set MEM_COL "\$10"
    set NAME_COL "\$12"
    set -e columns_list
    if test -n "$_flag_name"; set columns_list $columns_list $NAME_COL; end
    if test -n "$_flag_pid"; set columns_list $columns_list $PID_COL; end
    if test -n "$_flag_cpu"; set columns_list $columns_list $CPU_COL; end
    if test -n "$_flag_mem"; set columns_list $columns_list $MEM_COL; end
    set selected_colums ( string join "," $columns_list )
    set HEADER_ROW 7
    set awk_string "NR > $HEADER_ROW {print $selected_colums}"
    
    ( top -b -n 1 | awk $awk_string )
end

function sum_by_name -a data
    for x in $data
        echo $x
    end
end

# argparse smth -- $argv
# pass all remaining arguments to the next function
set -l data ( get_data $argv )
sum_by_name $data
