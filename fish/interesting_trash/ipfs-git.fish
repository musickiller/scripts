#!/bin/fish

# This script is my research in git-via IPNS
#
# Result is that git wrapper isn't really needed,
# just have a remote per-user and treat all remotes as read-only.
#
# For more info read at "ISSUE!!!"

# settings
set -g debug 2
set gum_spinner globe

# constants
set ERROR_BAD_KEY 2

if test $debug -gt 0; echo debug is enabled at level $debug !; end
set script_dir ( dirname ( status -f ))
#no idea how to make this better(
source $script_dir/../util/test_is_none_or_empty.fish
set work_dir $PWD
set config_dir $work_dir/config
echo "Warning: working dir is $work_dir"
echo "and config dir is $config_dir"
echo "And you gave to deal with that for now =P"
set key_file_location $config_dir/ipns_key_name
set cid_file_location $config_dir/ipns_cid


function load_cid
    set cid ( cat $cid_file_location )
    
    if not test_is_none_or_empty "$cid"
        echo $cid
        return 0
    end

    set key ( load_key )
    set cid ( get_ipns_cid_from_key $key )
    read -P "Preserve ipns cid in configs? (y/N): " answer
    
    if test ( string lower $answer ) = y
        mkdir -p $config_dir
        echo $cid > $cid_file_location
    end

    echo $cid
end
    

function load_key
    # can't figure out the debug from function when it is called for set
    #if test $debug -gt 0; command echo enter ( status function ) ; end
    #if test $debug -gt 1; command echo argv = $argv; end
    set key ( cat $key_file_location )
    
    if not test_is_none_or_empty "$key"
        echo $key
        return 0
    end

    read -P "please enter ipns key name: " key
    read -P "Preserve ipns key name in config? (y/N): " answer
    
    if test ( string lower $answer ) = y
        mkdir -p $config_dir
        echo $key > $key_file_location
    end
    
    if test_is_none_or_empty $key
        echo "Do not use your personal key, enter specific name."
        return $ERROR_BAD_KEY
    end

    echo $key
end


function get_ipns_cid_from_key -a key_name
    # won't work if you have several similar names, for now
    set key_and_name ( ipfs key list -l | grep $key_name )
    echo string split " " $key_and_name
end


function ipfs-git
    if test $debug -gt 0; command echo enter ( status function ); end
    if test $debug -gt 1; command echo argv = $argv; end
    
    switch $argv[1]
        case init
            git init $argv[2..]
            set key ( load_key )
            ipns_publish_with_remount $key $work_dir
    # no end after cases, pal! =)
    end
end


function ipns_publish_with_remount -a key path
    if test $debug -gt 0; command echo enter ( status function ); end
    if test $debug -gt 1; command echo argv = $argv; end

    if test_is_none_or_empty $key
        echo "Do not use your personal key, enter specific name."
        return $ERROR_BAD_KEY
    end

    # ISSUE!!!!
    # Even if I pull from $cid.ipns.localhost:8080 here
    # just before the add,
    # there is still a race condition, with ipns being a very slow guy
    # So no matter what I try, everyone will always owerwrite each other's changes(((
    # And I don't think there is any fix, unles we have some separate merging
    # mechanism without using git itself
    #
    # And that would also kinda elliminate the need for git for very basic cooperation
    # It can then be used of course, but first, some resolution of IPFS races needed
    #
    #
    #
    # The SOLUTION that I see now is that everyone has he's own repo
    # One can easily have a remote for every person in cooperation,
    # hence pulling from the remote their branch is based upon
    #
    # And then the maintainer goes through PR's and pulls from branches of other users
    # Which is kinda the same as it normally is in git?
    #
    #
    #
    # So the TLDR here would be.... No git wrapper needed, instead a Hub/Lab 
    # should work with all remotes being read-only.
    set hash ( ipfs add -Qr $path/.git )
    if test $debug -gt 0; command echo repo hash is $hash; end
    
	gum spin \
	    --spinner $gum_spinner \
	    --title "Unmounting ipns..." \
	    -- fusermount -u /ipns
    # can't make it print the ipns key (or is it extraneous? it's the same always...)
	gum spin \
	    --spinner $gum_spinner \
	    --title "Publishing..." \
	    -- ipfs name publish --key=$key $hash
	gum spin \
	    --spinner $gum_spinner \
	    --title "Mounting ipfs+ipns..." \
	    -- ipfs mount
end


function ipfs_generate_key -a name
    ipfs key gen $name
end


ipfs-git $argv
