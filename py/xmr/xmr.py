import json

class XmrigConfig:
    def __init__(self, path_to_config: str="/root/.config/xmrig.json"):
        self.path_to_config=path_to_config

    def load(self) -> dict:
        with open(self.path_to_config) as f:
            data = json.load(f)
        return data

    def cat(self):
        print(json.dumps(self.load(), indent=2))

    def change_cpu(self, percent: int = 100):
        data = self.load()
        data["cpu"]["max-threads-hint"] = percent
        self.save(data)
        
    def save(self, data):
        with open(self.path_to_config, "w") as f:
            json.dump(data, f, indent=2)
